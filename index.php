<!DOCTYPE HTML>

<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	    <meta name="description" content="Official Website Himatika Unuja" />
	    <meta name="keywords" content="unuja himatika event" />
		<title>Himatika - Unuja</title>	
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
	
		<link rel="stylesheet" href="css/animate.min.css" />
		<link rel="stylesheet" href="css/skel.css" />
		<link rel="stylesheet" href="css/style.css" />
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/wow.js"></script>
		<script src="js/init.js"></script>
   
		
		
	</head>
		<body>
			<section id="banner">
				<div class="inner">
					<h2 class="wow fadeInDown">himatika event 2018</h2>
            		<p class="text-faded">Let's Start Skill Programming For Your Experience</p>
					
					<a href="#one" class="button alt wow fadeInUp page-scroll actions">More</a>
				</div>
			</section>
		

		



<!-- One -->
			<section id="one" class="wrapper style1">
				<header class="major">
					<h2 class="wow fadeInDown">WEB DESIGN COMPETITION</h2>
				</header>
				<div class="inner">
					<p class="text-faded  wow fadeInDown justify">
                      Gabung dan Daftarkan Dirimu Segera Untuk Mengikuti Himatika Event 2018. Saatnya Tunjukkan Skill Programmu dan Bawa Pulang Hadiahmu </p>
                    <p style="font-size: 2.5em ; ">25 - 26 April 2018</p>
					<ul class="actions">
						<li><a href="http://event.himatikaunuja.com/user/home" target="_blank" class="button big alt wow fadeInUp">Ayo gabung</a></li>
					</ul>
				</div>
			</section>
			
		<!-- Two -->
			<section  class="wrapper style1">
				<header class="major">
					<h2>SUPPORTED</h2>
				</header>
				<div class="container">
					<div class="row">
						<div class="4u">
							<section class="special box">
								<img src="images/nurja.png" alt="" class="icon major">
								<h3><a href="http://nuruljadid.net" target="_blank">Ponpes. Nurul Jadid</a></h3>
								<p class="wow fadeInUp">Salah satu pesantren terbesar di Desa Karanganyar, Paiton, Probolinggo. didirikan pada tanggal 10 Muharam  1368 H / 12 November 1948 M oleh KH. Zaini Mun’im</p>
							</section>
						</div>
						<div class="4u">
							<section class="special box">
								<img src="images/unuja.png" alt="" class="icon major">
								<h3><a href="http://unuja.ac.id" target="_blank" >univ. nurul jadid</a></h3>
								<p class="wow fadeInUp">Universitas yang berusaha memadukan antara IPTEK dan IMTAQ sesuai dengan status yang disandangnya, yaitu lembaga pendidikan tinggi yang secara historis lahir di lingkungan pesantren</p>
							</section>
						</div>
						<div class="4u">
							<section class="special box">
								<img src="images/himatika.jpg" alt="" class="icon major">
								<h3><a href="#">himatika</a></h3>
								<p class="wow fadeInUp">Salah satu himpunan mahasiswa program studi informatika fakultas teknik Universitas Nurul Jadid</p>
							</section>
						</div>
						
					</div>
				</div>
			</section>
			
		
		
<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="copyright">
						<li>&copy; 2018 Himatika</li>
						<li><a href=""><i class="icon fa-facebook"></i></a></li>
					</ul>
				</div>
			</footer>

	</body>
</html>