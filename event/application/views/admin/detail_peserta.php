<section  class="wrapper style1">
  <header class="major">
    <h2>DETAIL PESERTA</h2>
  </header>
	<div class="container">
		<div  class="confirm-page">
		
    <table>
      <tbody>
        <tr>
          <td><b>ID Peserta</b></td>
          <td>: <? echo $peserta->id_user;?></td>
        </tr>
        <tr>
          <td><b>Nama Lengkap</b></td>
          <td>: <? echo $peserta->nama;?></td>
        </tr>
        <tr>
          <td><b>Kelamin</b></td>
          <td>: <? echo  $peserta->kelamin == 'l' ? 'Pria' : 'Wanita';?></td>
        </tr>
        <tr>
          <td><b>Tgl Lahir</b></td>
          <td>: <? echo $peserta->tgl_lahir;?></td>
        </tr>
        <tr>
          <td><b>Alamat</b></td>
          <td>: <? echo $peserta->alamat;?></td>
        </tr>
        <tr>
          <td><b>Nama Sekolah</b></td>
          <td>: <? echo $peserta->nama_sekolah;?></td>
        </tr>
        <tr>
          <td><b>Alamat Sekolah</b></td>
          <td>: <? echo $peserta->alamat_sekolah;?></td>
        </tr>
        <tr>
          <td><b>Telephon</b></td>
          <td>: <? echo $peserta->telp;?></td>
        </tr>
        <tr>
          <td><b>Status Peserta</b></td>
          <td>: <? echo $peserta->status == 't' ? '<a href="'.base_url().'admin/approve_pembayaran/'.$peserta->id_user.'" class="btn">Konfirmasi</a>' : '<a style="color: green;">di Konfirmasi</a>';?></td>
        </tr>
        <tr>
          <td colspan="2"><a class="btn" href="<? echo base_url()?>admin/delete_user/$peserta->id_user">Hapus Peserta</a></td>
        </tr>
      </tbody>
    </table>
		</div>
	</div>
</section>

