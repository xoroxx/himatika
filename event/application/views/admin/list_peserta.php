<section  class="wrapper style1">
  <header class="major">
    <h2>LIST PESERTA</h2>
  </header>
	<div class="container">
		 <table   class="display datatables" width="100%" data-page-length="10" data-order="[[ 2, &quot;asc&quot; ]]">
        <thead>
          <th>NAMA</th>
          <th>KELAMIN</th>
          <th>NAMA SEKOLAH</th>
          <th>STATUS KONFIRMASI</th>
        </thead>
        <tbody>
          <? 
            foreach ($peserta as $row) {
             
              $kelamin = $row->kelamin == 'l' ? 'Pria' : 'Wanita';
              $status = $row->status == 't' ? '<a style="color: blue;">Belum</div>' : '<div style="color: green;">Sudah</div>';
              echo "<tr>

              <td><a href='".base_url()."admin/detail_peserta/".$row->id_user."'>".strtoupper($row->nama)."</a></td>
              <td>".$kelamin."</td>
              <td>".$row->nama_sekolah."</td>
              <td>".$status."</td>
              </tr>";
            }
          ?>
        </tbody>
      </table>
	
	</div>
</section>

