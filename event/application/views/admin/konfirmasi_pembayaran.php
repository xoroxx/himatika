<section  class="wrapper style1">
  <header class="major">
    <h2>KONFIRMASI PEMBAYARAN</h2>
  </header>
	<div class="container">
      <? if (!$peserta == FALSE) :?>
		  <table  class="display datatables" width="100%" data-page-length="10" data-order="[[ 0, &quot;asc&quot; ]]">
      
        <thead>
          <th>NAMA</th>
          <th>SEKOLAH</th>
          <th>BUKTI PEMBAYARAN</th>
          <th>OPSI</th>
        </thead>
        <tbody>
          <? 
            foreach ($peserta as $row) {
              
              $kelamin = $row->kelamin == 'l' ? 'Pria' : 'Wanita';
              $status = $row->status == 't' ? '<div style="color: blue;">belum dikonfirmasi</div>' : '<div style="color: green;">dikonfirmasi</div>';
              echo "<tr>

              <td><a href='".base_url()."admin/detail_peserta/".$row->id_user."'>".strtoupper($row->nama)."</a></td>
              <td>".$row->nama_sekolah."</td>
              <td class='align-center'><a class='btn' href='".base_url()."uploads/".$row->id_user."/".$row->foto."'>DOWNLOAD</a></td>
              <td class='align-center'><a  class='btn' href='".base_url()."admin/approve_pembayaran/".$row->id_user."' >konfirmasi</a></td>
              </tr>";
            }
          ?>
        </tbody>
      </table>
      <? endif;?>
      
		</div>
	</div>
</section>

