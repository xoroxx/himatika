<section id="main" class="wrapper style1">
  <header class="major">
    <h2>DAFTAR PERSERTA</h2>
    <p>Lengkapi Semua Kolom Yang Ada</p>
  </header>
  <div class="container">
      <div class="form">
        <form action="do_register" id="regForm"  method="post" >
          <div class="tab">Data Peserta :
            <? echo form_error('n_depan')?>
            <input type="text"  placeholder="Nama Depan"  name="n_depan" >
            <input type="text" placeholder="Nama Belakang" name="n_belakang">
            <select name="kelamin" id="">
              <option value="l">Pria</option>
              <option value="p">Wanita</option>
            </select>
          </div>
          <div class="tab">Tanggal Lahir:
            <? echo form_error('dd')?>
            <input type="text" placeholder="dd" oninput="this.className = ''" name="dd">
            <input type="text" placeholder="mm" oninput="this.className = ''" name="mm">
            <input type="text" placeholder="yyyy" oninput="this.className = ''" name="yyyy">
          </div>
  <div class="tab">Alamat:
    <? echo form_error('alamat')?>
    <textarea name="alamat" id="" cols="30" rows="3" placeholder="Alamat Lengkap"></textarea>
    <input type="text" placeholder="Telepon " oninput="this.className = ''" name="telp">
  </div>
  <div class="tab">Sekolah Asal:
    <? echo form_error('s_nama')?>
    <input type="text" placeholder="Nama Sekolah" oninput="this.className = ''" name="s_nama">
    <textarea  name="s_alamat"  placeholder="Alamat" cols="30" rows="3"></textarea>
    
  </div>
  
  <div class="tab">Data Akun:
    <? echo form_error('email')?>
    <input type="email" placeholder="Email"  name="email">
    <? echo form_error('password')?>
    <input type="password" placeholder="Password" name="password">
  </div>
  <div style="overflow:auto;">
    <div>
      <button style="margin-top: 15px" type="button" id="nextBtn" onclick="nextPrev(1)">Lanjut</button>
      <p id="prevBtn" ><a href="#" onclick="nextPrev(-1)">Kembali</a></p>
    </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
  </div>
</form>
</div>
  </div>
</section>      

        
<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "daftar";
  } else {
    document.getElementById("nextBtn").innerHTML = "Lanjut";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
  
    return false;

    
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>