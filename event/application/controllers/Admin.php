<?php
 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
/**
 * Description of home
 *
 * @author Admin
 */
class Admin extends CI_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->Library(array('template','session','form_validation'));
        $this->load->helper(array('html_helper','url_helper','form_helper'));
        $this->load->model('Mdl_admin');
        $this->template->set_template('admin');
    }
 
    public function index() {
        if ($this->session->userdata('adminLogin') == TRUE) {
            redirect('admin/list_peserta');
        }else{
            redirect('admin/login');
        }
    }
 	
 	/**
 	 * [delete_user controller]
 	 * @return [type] [description]
 	 */
    public function delete_user()
    {
    	$id_user = $this->uri_>segment(3);
    	$result = $this->Mdl_admin->delete_user($id_user);
    	if ($result === TRUE) {
    		redirect('admin/list_peserta');
    	}else{
    		echo "<script>
    			alert('Something Error');
    			history.go(-1);
    		</script>";
    	}
    }

    public function login($value='')
    {
       if ($this->session->userdata('adminLogin') == FALSE) {

        $this->form_validation->set_rules('username', 'username', 'required|trim', array('required' => '* %s tidak boleh kosong'));
        $this->form_validation->set_rules('password', 'Password', 'required|trim', array('required' => '* %s tidak boleh kosong'));
        $this->form_validation->set_error_delimiters('<span class="text-error">', '</span>');

        if($this->form_validation->run() == FALSE){
            $this->template->set_master_template('template');
            $this->template->write_view('content', 'admin/login', TRUE);
            $this->template->render();
        }else
        {
            $data = array(
                'username' => $this->input->post('username'),
                'password' => sha1($this->input->post('password'))
            );
           
            $result =  $this->Mdl_admin->login($data);
            
            if($result > 0)
            {   
                $session = array(
                    'adminLogin' => TRUE, 
                    'id_admin' => $result->id_admin
                );
                $this->session->set_userdata($session);
                redirect('admin/list_peserta');
            }else{
                echo " <script>
                    alert('Pastikan Email dan Password Anda benar!');
                    history.go(-1);
                  </script>"; 
            }
        }
       }
    
      
    }

    public function approve_pembayaran()
    {
        $id_user = $this->uri->segment(3);
        $this->Mdl_admin->approve_pembayaran($id_user);
        $this->Mdl_admin->approve_peserta($id_user);
        redirect('admin/list_peserta');
    }


    public function list_peserta($value='')
    {
        if ($this->session->userdata('adminLogin') == FALSE) {
            redirect('admin/login');
        }else{
            //$data = array('status' => 't' );
            $data['peserta'] = $this->Mdl_admin->get_peserta();
            $this->template->write_view('content', 'admin/list_peserta', $data, TRUE);
            $this->template->render();
        }
    }
    /*
    
     */
    public function detail_peserta()
    {
        if ($this->session->userdata('adminLogin') == TRUE) {
            $id = $this->uri->segment(3);
            $data['peserta'] = $this->Mdl_admin->get_detail_peserta($id);
            $this->template->write_view('content', 'admin/detail_peserta', $data, TRUE);
            $this->template->render();
        }
    }

    public function konfirmasi_pembayaran()
    {
        if ($this->session->userdata('adminLogin') == FALSE) {
            redirect('admin/login');
        }else{

            $data['peserta'] = $this->Mdl_admin->get_pembayaran('t');
            $this->template->write_view('content', 'admin/konfirmasi_pembayaran', $data, TRUE);
            $this->template->render();
        }
    }

    public function data_pembayaran()
    {
        if ($this->session->userdata('adminLogin') == FALSE) {
            redirect('admin/login');
        }else{

            $data['peserta'] = $this->Mdl_admin->get_pembayaran('y');
            $this->template->write_view('content', 'admin/data_pembayaran', $data, TRUE);
            $this->template->render();
        }
    }

    public function show_img()
    {
        if ($this->session->userdata('adminLogin') == FALSE) {
            redirect('admin/login');
        }else{
            $id = $this->uri->segment(3);
            $image = $this->Mdl_admin->get_img($id);
            //header("Content-type: image/jpeg");
            $this->load->helper('download');
            $data = file_get_contents(base_url('/uploads/'.$id.'/'.$image->foto));
            force_download($data, $data);
        }
    }

    public function profil()
    {
        if ($this->session->userdata('adminLogin') == FALSE) {
            redirect('admin/login');
        }else{  
            $id = $this->session->userdata('id_admin');
            $data['admin'] = $this->Mdl_admin->get_admin($id);
            $this->template->write_view('content', 'admin/profil', $data, TRUE);
            $this->template->render();  
        }
    }
    public function logout($value='')
    {
        $this->session->sess_destroy();
        redirect('admin/login');
    }
}
 
/* End of file home.php */
/* Location: ./application/controllers/home.php */