<?php 
/**
* 
*/
class Mdl_user extends CI_Model
{
	function __construct()
	{
		$this->load->database();
	}

	public function get($id_akun = false)
	{
		$this->db->from($this->table);
	    if ($id_akun === FALSE)
	    {
	      $this->db->order_by('nama_sekolah', 'ASC');
	      return $this->db->get()->result();
	    }
	    $this->db->where('id_akun', $id_akun);
	    return $this->db->get()->row();
	    $this->db->close();
	}

    public function cek_user($data)
	{
		$this->db->from('user');
	    $this->db->where('email', $data);
	    return $this->db->get()->row();
	}
	
	
	public function create($data = array())
	{
		return $this->db->insert('user', $data);
	}

	public function login($data = array())
  	{
	    $this->db->from('user');
	    $this->db->where($data);
	    return $this->db->get()->row();
  	}

  	public function register($data = array())
  	{
	    return $this->db->insert('user', $data);
  	}

  	public function get_user($value='')
  	{
	    $this->db->from('user');
	    $this->db->where('id_user', $value);
	    return $this->db->get()->row();
  	}

  	public function cek_pembayaran($data  = '')
  	{
	    $this->db->from('pembayaran');
	    $this->db->where('id_user', $data);
	    return $this->db->get()->row();
  	}
  	public function get_count_peserta($data = array())
  	{
  		$this->load->database();
	    $this->db->from('peserta');
	    $this->db->where($data);
	    return $this->db->get()->num_rows();
  	}

  	public function adddelegasi($data = array())
  	{
  		return $this->db->insert('peserta', $data);
  	}

  	public function deldelegasi($value='')
  	{
  		$this->db->from('peserta');
  		$this->db->where('id_peserta', $value);
  		return $this->db->delete();
  	}

  	public function do_confirm($data = array())
  	{
  		return $this->db->insert('pembayaran', $data);
  	}
}